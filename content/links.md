---
title: Links
menus:
  main:
    weight: 200
tags: []
date: 2022-01-02T20:30:36+01:00
---
Links to other cool blogs and other interesting sites.

# [Sijisu](https://sijisu.eu)
My friend, classmate and partner-in-crime for almost everything I do. He can do
everything I can, just better. He also does cybersecurity, but at a much deeper 
level than me and that's what most his blog posts are about. I don't know much
more to say. He's cool af. Check him out. 

# [Vojta Káně](https://vkane.cz)
We also met in school, but he's two years older than me. He led me through my
first steps in computer science and Linux and taught me a big part of what I
know. Even now, whenever we talk, I always learn so much information about an
interesting topic (and usually almost suffocate laughing). He is a scout, nixOS user,
freedom and openness madman and a really good programmer. Not only can he code
almost anything, he can do it *well*. 

# [Pátek.cz](https://patek.cz)
Club/makerspace in my school. There might be some articles or talks by me. I
also talk my involvement with them [here]({{% relref "../about/making/patek/" %}}),
[here]({{% relref "../about/making/web/patek-cz/"%}}) and [here]({{% relref "../about/graphics/patek/" %}}).

# [KSP](https://ksp.mff.cuni.cz)
A Czech programming contest, I sometimes take part in. Their website has a nice
programming course and contains loads of computer science knowledge.

<div style="display: none">
Ah! You found my easter egg! Here's you reward, 
an <a href="https://labutejsounej.eu">absolutely based blog</a> written by a girl I
know. It contains teen twitter shitposts sprinkled with amazingly broad
knowledge of technology and networking, knitting, and recipes for all kinds of
food. All of that is packaged in a beautifully broken 90s-style pure HTML
website. Caution: May contain weird unicode characters, cotton and swans.
</div>


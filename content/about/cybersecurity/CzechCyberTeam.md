---
title: "CzechCyberTeam"
weight: 200
---
I've particiapted in the [Czech CyberSecurity Contest](https://kybersoutez.cz)
every year since 2018. I qualified for the finals each time and last year (2021)
I managed to secure second place. 

The Czech Cyber Team is formed from the top contestants in the CCSC and its main
purpouse is participation in the [European Cyber Security Challenge](https://ecsc.eu).
I haven't been to ECSC as a contestant yet, because in 2020 it was cancelled due
to COVID and in 2021 it [took place in Prague]({{< relref "ECSC2021" >}}) and I
worked with the rest of the Czech team as an organizer. 

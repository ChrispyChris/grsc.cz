---
title: "ECSC2021"
weight: 300
image:
  src: "IMG_0052.jpg"	
  alt: "Contest room with the scoreboard at the end of Day 1 of ECSC 2021."
---
In 2021, the [European CyberSecurity Challenge](https://ecsc.eu) took place [in Prague](https://ecsc2021.cz). The Czech team made a decision to not participate in the competition, instead we helped with organising the contest -- created challenges, worked on the infrastructure, provided support. 

I made two challenges -- *The Bomb* and *Covid Pass(word)*:

### The Bomb

This was a hardware challenge, where the competitors had access to a remote terminal through a TCP socket. When connecting to the socket, the output was similar to this: 

```
$$7522fa9277a94d93$wait$d407b370$$
BOMB ok wait T-4992s
$$11b1d136d2161099$wait$25ebcc12$$
BOMB ok wait T-4989s
$$933a777a962fa134$wait$9eb7837c$$
BOMB ok wait T-4986s
```

The goal was to understand how the communication worked and send a command, which would defuse the bomb. 

The backend is written in Go and there was no actual hardware involved; it was all simulated. I must say it was an absolute pleasure to write the communication of the virtual devices using goroutines and channels. 

### Covid Pass(word)

It was tagged as *reversing*, but thinking back it probably should have been *misc*. The challenge involved a website with a webcam view, which scanned QR codes and sent them to a backend, also written in Go, which validated EU Covid Certificates. The goal was to find a vulnerability in my code, which allowed to submit any arbitrary data in the certificate without the certificate being considered invalid.

{{< blogPhoto src="image-20211018102812683.png" alt="Screenshot of Covid Pass(word) in action" >}}

I also worked on the dynamic scoreboard:

{{< blogPhoto src="IMG_0052.jpg" alt="Contest room with the scoreboard at the end of Day 1">}}


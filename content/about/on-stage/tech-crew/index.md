---
title: "Tech crew"
weight: 100
---

You can sometimes see me behind the stage of some small concert moving around large cases. That's because one of my friends works as a stage lighting and sound engineer and I often join him to help. This means I have experience with setting up gigs, configuring everything and even some basic knowledge of how to control lights using an Avolites Titan. 

<figure>
    <video controls>
        <source src="lights.mp4">
    </video>
    <figcaption>Recording of an absolute bop of a song with lights by me.</figcaption>
</figure>


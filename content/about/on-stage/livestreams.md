---
title: "Livestreams"
weight: 300
---

The same guy who I do [lights and sound]({{< relref  "tech-crew" >}}) with, also does livestreams for sport events, fairs, town council meetings and other events. I often work with him as a camera operator or a backup "director" and I help him setup everything. He uses vMix and a whole array of different wired or wireless video and audio transmitting devies. 


---
title: "classroom.grsc.cz"
weight: 100
image:
  src: "classroom.png"
  alt: "Screenshot of classroom.grsc.cz."
---

This is one of my latest web projects. During lockdown, our school relied on Google Classroom to distribute work to students. This meant that every day I stared into the ToDo view, which, quite frankly, sucks ass. There are three main problems with it: 

1. You can't view all assignments, they are grouped by week and when you expand one group, the other get closed.
2. The week begins with a Sunday. 
3. And most importantly, assignments get hidden after their due date and moved into a different view, which doesn't indicate it in any way. This means that if you only use the ToDo view, you can very easily miss these assignments and realize they're missing much much later.

This annoyed me so much, I created my own website, which fetches assignments from the Classroom API and displays them in an actually reasonable way. The app is available on [classroom.grsc.cz](https://classroom.grsc.cz) and its source code is on [GitHub](https://github.com/Greenscreener/classroom.grsc.cz). 

{{< blogPhoto src="classroom.png" alt="Screenshot of classroom.grsc.cz." >}}
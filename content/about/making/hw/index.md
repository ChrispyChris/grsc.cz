---
title: "Hardware"
weight: 300
image:
  src: "buttons-back.jpg"
  alt: "Back side of one of my projects - the arduino FBV pedal."
---

I like playing around with hardware, I have experience with Arduino, ESP, I've worked briefly with an STM Nucleo. 

### USBSerialKeyboard

This is quite an old project of mine. I created a device with a DigiSpark and a USB to Serial converter, which types whatever you send to it through serial on a virtual Keyboard. The main use case is typing long passwords from password managers into computers from your phone. 

{{< blogPhoto src="usbserialkeyboard.jpg" alt="Photo of USBSerialKeyboard." >}}

### Sprinklers

A device which interfaces with a Hunter irrigation system using their  proprietary *ROAM* connector. I found the source code for the protocol in a comment of some old hackster.io article. I also write a simple PWA to interface with the device. I have to get around to polishing it, but right now, it only works about 50 % of the time, so the source code for it also isn't available right now. 

{{< blogPhoto src="sprinkler-esp.jpg" alt="Circuit for the sprikler controller with a full bridge rectifier, a step down regulator, an ESP and an Arduino." >}} 

### Arduino FBV Pedal

I have a Line 6 modelling amp at home. It can do all kinds of effects, but it's very hard, almost impossible, to switch effects while playing. Line 6 sells a remote control pedal board, but it is very expensive for a glorified set of a few buttons. So I decided to try to make my own. 

I found a [great blog post](https://alan.ferrency.com/2017/10/05/line6-fbv-part-2/) by Allan Ferrency which details his attempt at something very similar, although for a different amp. This helped me very much, because after mocking up the device on a breadboard, it just started working and I only had to figure out all the codes for the different buttons my amp uses. I also ended up implementing the RS-422 protocol using MAX485s, because I had them on hand from a previous project. 

The source code and circuit diagrams are available on [GitLab](https://gitlab.com/Greenscreener/arduino-fbv-pedal/).

I also made a casing for it from wood, which was probably the hardest part of the project, as I have no experience with woodworking. 

{{< blogPhoto src="buttons.jpg" alt="Front side of the pedal board." >}}

{{< blogPhoto src="buttons-back.jpg" alt="Back side of the pedal board." >}}

### Freeform electronics

Inspired by [Jiří Praus](https://twitter.com/jipraus), I started making glowing jewelry made from SMD LEDs and wire and I ended up making quite a few.

{{< blogPhoto src="freeform.jpeg" alt="Some of the different designs of glowing jewelry I made." >}}


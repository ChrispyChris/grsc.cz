---
title: "Go"
weight: 200
---

I quite recently started writing more and more in Go and I feel like I've finally started getting the hang of it. I wrote my two challenges for [ECSC2021]({{< relref "../cybersecurity/ECSC2021" >}}) in Go. I also participate in programming contests like [KSP](https://ksp.mff.cuni.cz) or [kasiopea](https://kasiopea.matfyz.cz) and a large majority of my solutions are written in Go. You can find them [on Gitlab](https://gitlab.com/Greenscreener/ksp-public). (This section doesn't contain much (yet), but I'm looking forward to expanding it.) 

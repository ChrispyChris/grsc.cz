---
title: "This Site"
weight: 150
---

To be honest, it is probably way more complex than it needs to be. I use the Hugo static site generator to generate the website from a simple template I made myself. I tried to optimize for download size and page load time, which also made the task of creating this a bit harder. 

I use the `blogPhoto` [shortcode](https://gitlab.com/patek-devs/patek.cz/-/blob/master/themes/patek/layouts/shortcodes/blogPhoto.html) written by [Vojtěch Káně](https://vkane.cz) for Pátek's website and the system for pregenerating FontAwesome icons is inspired by [this blog post](https://www.client9.com/using-font-awesome-icons-in-hugo/) by Nick Galbreath. 

Also, please excuse any grammatical or stylistic errors, English isn't my first language and especially commas are still a bit of a challenge for me - I tend to haphazardly apply Czech comma rules and that really doesn't work. If you found an error, please don't hesitate to drop me an issue or merge request in the [repository](https://gitlab.com/Greenscreener/grsc.cz). 

The source code of the site is available under the MIT license and all the content is under [CC-BY-4.0](https://creativecommons.org/licenses/by/4.0/).


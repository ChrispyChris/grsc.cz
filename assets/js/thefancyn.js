function distance(p1, p2) {
    return Math.sqrt((p1.x-p2.x)**2 + (p1.y-p2.y)**2);
}
function directionalScale(p1, value) {
    if (Math.abs(p1.x) >= Math.abs(p1.y)) {
        const k = Math.abs(p1.x)/Math.abs(p1.y);
        const output = {};
        output.x = Math.sign(p1.x)*value;
        output.y = Math.sign(p1.y)*value*k;
        return output;
    } else {
        const k = Math.abs(p1.y)/Math.abs(p1.x);
        const output = {};
        output.y = Math.sign(p1.y)*value;
        output.x = Math.sign(p1.x)*value*k;
        return output;
    }
}
class N {
    constructor(world, mass=1, innerHTML="n", x=0, y=window.innerHeight - 43, bounce=0.7) {
        this.pos = {};
        this.vel = {};
        world.addObject(this);
        this.world = world;
        this.mass = mass || 1;
        this.pos.x = x || 0;
        this.pos.y = y || window.innerHeight - 43;
        this.vel.x = 0;
        this.vel.y = 0;
        this.bounce = bounce;
        this.element = document.createElement("div");
        this.element.classList.add("anN");
        this.element.innerHTML = innerHTML;
        document.body.appendChild(this.element);

    }
    draw() {
        this.vel.y -= this.world.gravity;
        this.vel.x -= this.vel.x < 0 ? this.mass**-1 * -this.world.airResistance * this.vel.x**2 : this.mass**-1 * this.world.airResistance * this.vel.x**2;
        this.vel.y -= this.vel.y < 0 ? this.mass**-1 * -this.world.airResistance * this.vel.y**2 : this.mass**-1 * this.world.airResistance * this.vel.y**2;
        this.pos.x += this.vel.x;
        this.pos.y += this.vel.y;
        if (this.pos.x < 0 || this.pos.x > (window.innerWidth - 20)) {
            this.vel.x = -this.vel.x*this.bounce;
            this.vel.y = this.vel.y*this.bounce + 0.5*this.vel.y*this.bounce*(Math.random() - 0.5);
            if (this.pos.x < 0) {
                this.pos.x = 0;
            } else {
                this.pos.x = window.innerWidth - 20;
            }
        }
        if (this.pos.y < -9 || this.pos.y > (window.innerHeight - 25)) {
            this.vel.y = -this.vel.y*this.bounce;
            this.vel.x = this.vel.x*this.bounce + 0.5*this.vel.x*this.bounce*(Math.random() - 0.5);
            if (this.pos.y < -9) {
                this.pos.y = -9;
            } else {
                this.pos.y = window.innerHeight - 25;
            }
        }
        /*if (mouseDown) {
            if (distance(this.pos, {x: cursorX, y: cursorY}) > 5) {
                const force = directionalScale({
                    x: cursorX - this.pos.x,
                    y: cursorY - this.pos.y
                }, distance(this.pos, {x: cursorX, y: cursorY}) ** -1);
                this.vel.x += force.x * 1;
                document.getElementById("xForce").value = force.x+1;
                this.vel.y += force.y * 1;
                document.getElementById("yForce").value = force.y+1;
            } else {
                this.vel.x = 0;
                this.vel.y = 0;
            }
        }*/
        if (mouseDown) {
            this.nudge((cursorX - this.pos.x)/50, (cursorY - this.pos.y)/50);
        }
        this.element.style.left = this.pos.x + "px";
        this.element.style.bottom = this.pos.y + "px";
    }
    nudge(x=0,y=0) {
        this.vel.x += x;
        this.vel.y += y;
    }

}
class World {
    draw() {
        this.objects.forEach(e => e.draw());
    }
    addObject(object) {
        this.objects.push(object);
    }
    constructor(gravity=1, airResistance) {
        this.gravity =  gravity;
        this.airResistance = airResistance || 0.001;
        this.objects = [];
    }
}
let world;
let n;
let cursorX;
let cursorY;
let mouseDown = false;
let mouseDownEver = false;
document.onmousedown = function(e) {
    if (e.button === 0) {
        mouseDown = true;
		if (!mouseDownEver) {
			setInterval(() => {world.draw()}, 10);
		}
        mouseDownEver = true;
    }
};
document.onmouseup = function(e) {
    if (e.button === 0) {
        mouseDown = false;
    }
};
document.addEventListener("DOMContentLoaded", () => {
    world = new World();
    n = new N(world);
	n.pos.x = window.innerWidth/2
	n.pos.y = window.innerHeight/2
	world.draw()
});
document.addEventListener("mousemove", (e) => {
    cursorX = e.pageX;
    cursorY = window.innerHeight - e.pageY;
});
